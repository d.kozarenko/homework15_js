"use strict";
let userNumber;
do {
    userNumber = prompt("Enter number please: ", userNumber);
} while (userNumber.trim() === "" || isNaN(userNumber));
console.log(factorialCalc(+userNumber));
function factorialCalc(number){
    if (number === 1){
        return number;
    } else {
    return number * factorialCalc(number - 1);
    }
}